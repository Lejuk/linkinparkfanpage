import React from "react";
import { Link } from "react-router-dom";

export default function MainScreen() {
  return (
    <div className="main-container">
      <div className="main-page">
        <div className="main-page-albums">
          <div className="main-page-albums-item">
            <img
              src="https://www.linkinpark.com/sites/g/files/g2000010631/files/2020-08/9jhshkfnhsla.png"
              alt=""
            />
            <ul>
              <li>HYBRID THEORY</li>
              <li>REANIMATION</li>
              <li>DOUBLE LP</li>
            </ul>
            <Link className="go-to-albums" to="/albums">
              go to albums
            </Link>
          </div>
          <div className="main-page-albums-item">
            <img
              src="https://www.linkinpark.com/sites/g/files/g2000010631/files/2020-08/8jkshagbkksx.png"
              alt=""
            />
            <ul>
              <li>HYBRID THEORY</li>
              <li>B-SIDE RARITIES</li>
              <li>16 PAGE BOOKLET</li>
            </ul>
            <Link to="/albums" className="go-to-albums">
              go to albums
            </Link>
          </div>
        </div>

        <div className="main-page-merch">
          <div className="main-page-merch-item">
            <img
              src="https://www.linkinpark.com/sites/g/files/g2000010631/files/2020-08/ht983kksi98k3hs.png"
              alt=""
            />
            <Link to="/contacts">
              <button>buy merch</button>
            </Link>
          </div>
          <div className="main-page-merch-item">
            <img
              src="https://cdn.shopify.com/s/files/1/2117/2713/products/LPIPins2_grande.png?v=1603411956"
              alt=""
            />
            <Link to="/contacts">
              <button>buy merch</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
