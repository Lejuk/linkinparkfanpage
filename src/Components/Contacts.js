import React from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { Icon } from "leaflet";

export default function Contacts() {
  return (
    <div>
      <h1 className="contacts-header">Offline Merch Store Location</h1>
      <MapContainer
        center={[34.052238, -118.24334]}
        zoom={13}
        scrollWheelZoom={false}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={[34.04294191849442, -118.26730778287232]}>
          <Popup>
            The Linkin Park Offline Fan Shop
            <br />
            1111 S Figueroa St, Los Angeles, CA 90015, USA <br />
          </Popup>
        </Marker>
      </MapContainer>
    </div>
  );
}
