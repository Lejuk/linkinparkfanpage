import React from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function Header() {
  const settings = {
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 2500,
    autoplaySpeed: 3500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  };
  return (
    <header className="header-cotainer">
      <ul className="header-container-menu">
        <li id="mainNav" className="header-container-menu-item">
          <Link to="/">main page</Link>
        </li>
        <li id="galleryNav" className="header-container-menu-item">
          <Link to="/gallery">gallery</Link>
        </li>
        <li id="albumsNav" className="header-container-menu-item">
          <Link to="/albums">albums</Link>
        </li>
        <li id="eventsNav" className="header-container-menu-item">
          <Link to="/upcoming events">upcoming events</Link>
        </li>
        <li id="contactsNav" className="header-container-menu-item">
          <Link to="/contacts">contacts</Link>
        </li>
      </ul>

      <Slider {...settings}>
        <div>
          <img
            className="sliderImage"
            src="https://i.pinimg.com/originals/53/7a/60/537a6011b85f9a95378ad4835152a9fd.jpg"
          />
        </div>
        <div>
          <img
            className="sliderImage"
            src="https://lifeimg.pravda.com/images/doc/8/7/878c5d0-chester-bennington1320.jpg"
          />
        </div>
        <div>
          <img
            className="sliderImage"
            src="https://cdn.ananasposter.ru/image/cache/catalog/poster/music/87/7172-1000x830.jpg"
          />
        </div>
        <div>
          <img
            className="sliderImage"
            src="https://homido.ru/upload/iblock/af1/8eddada8f68bcc0deadcb4d196ddb344.jpg"
          />
        </div>
        <div>
          <img
            className="sliderImage"
            src="https://images.ua.prom.st/1647706712_plakat-linkin-park.jpg"
          />
        </div>
      </Slider>
    </header>
  );
}
