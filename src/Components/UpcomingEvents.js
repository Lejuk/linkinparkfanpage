import React, { useState } from "react";
import Modal from "react-modal";

Modal.setAppElement("#root");
export default function UpcomingEvents() {
  
  const postCustomer = (user) => fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(user),
  }).then((response) => response.json());

  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    <div className="main-container-events">
      <div id="LA" className="main-container-events-item">
        <div className="main-container-events-item-hover">
          <h2>MAY 19</h2>
          <h1>chester tribute LINKIN PARK SHOW</h1>
          <h3>Staples Center, Los Angeles, CA</h3>
          <button onClick={() => setModalIsOpen(true)}>BUY TICKETS</button>
        </div>
      </div>
      <div id="NY" className="main-container-events-item">
        <div className="main-container-events-item-hover">
          <h2>MAY 27</h2>
          <h1>chester tribute LINKIN PARK SHOW</h1>
          <h3>MGM Grand, New York, NY</h3>
          <button onClick={() => setModalIsOpen(true)}>BUY TICKETS</button>
        </div>
      </div>
      <div id="MIAMI" className="main-container-events-item">
        <div className="main-container-events-item-hover">
          <h2>June 15</h2>
          <h1>chester tribute LINKIN PARK SHOW</h1>
          <h3>Ocean Drive Club, Miami, FL</h3>
          <button onClick={() => setModalIsOpen(true)}>BUY TICKETS</button>
        </div>
      </div>
      <div id="VEGAS" className="main-container-events-item">
        <div className="main-container-events-item-hover">
          <h2>June 22</h2>
          <h1>chester tribute LINKIN PARK SHOW</h1>
          <h3>Little Paris Club, Las Vegas, NV</h3>
          <button onClick={() => setModalIsOpen(true)}>BUY TICKETS</button>
        </div>
      </div>
      <div id="CLEVELAND" className="main-container-events-item">
        <div className="main-container-events-item-hover">
          <h2>June 31</h2>
          <h1>chester tribute LINKIN PARK SHOW</h1>
          <h3>Dark Side House Club, Cleveland, OH</h3>
          <button onClick={() => setModalIsOpen(true)}>BUY TICKETS</button>
        </div>
      </div>
      <div id="SANTABARBARA" className="main-container-events-item">
        <div className="main-container-events-item-hover">
          <h2>July 7</h2>
          <h1>chester tribute LINKIN PARK SHOW</h1>
          <h3>Callabasas Club, Santa Barbara, CA</h3>
          <button onClick={() => setModalIsOpen(true)}>BUY TICKETS</button>
        </div>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={() => setModalIsOpen(false)}
        style={{
          overlay: {
            width: "81%",
            margin: "0% auto auto",
            backgroundColor: "black",
            opacity: "0.9",
          },
          content: {
            backgroundColor: "black",
          },
        }}
      >
        <h1 className="form-header">Tickets Booking</h1>
        <form onSubmit={postCustomer()}>
          <label for="nameInput">Name</label>
          <br />
          <input name="nameInput" type="text" />
          <br />

          <label for="secondNameInput">Second Name</label>
          <br />
          <input name="secondNameInput" type="text" />
          <br />

          <label for="emailInput">Email</label>
          <br />
          <input name="emailInput" type="email" />
          <br />

          <label for="locationSelection">Event Location</label>
          <br />
          <select name="locationSelection">
            <option>Los Angeles</option>
            <option>New York</option>
            <option>Miami</option>
            <option>Las Vegas</option>
            <option>Cleveland</option>
            <option>Santa Barbara</option>
          </select>
          <br />

          <label for="numberInput">Number of Tickets</label>
          <br />
          <input name="numberInput" type="number" />
          <br />

          <button type="submit" className="submitFormButton">
            Book
          </button>
        </form>
      </Modal>
    </div>
  );
}
