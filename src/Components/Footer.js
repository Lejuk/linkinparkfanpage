import React from "react";

export default function Footer() {
  return (
    <footer className="footer-container">
      <div className="footer-container-content">
        <h1>linkin park</h1>
      </div>
      <div className="footer-container-inline-block">
        <ul className="footer-container-inline-block-menu">
          <li className="footer-container-inline-block-menu-item">
            <a href="">
              <img
                src="https://type8innovation.files.wordpress.com/2020/10/best-solutions-of-instagram-png-transparent-png-images-unique-white-instagram-logo-outline-of-white-instagram-logo-outline-copy.png"
                alt=""
              />
            </a>
          </li>
          <li className="footer-container-inline-block-menu-item">
            <a href="">
              <img
                src="https://iconsplace.com/wp-content/uploads/_icons/ffffff/256/png/facebook-icon-18-256.png"
                alt=""
              />
            </a>
          </li>
          <li className="footer-container-inline-block-menu-item">
            <a href="">
              <img
                src="https://www.nicepng.com/png/full/346-3465427_facebook-icon-twitter-icon-twitter-logo-white-box.png"
                alt=""
              />
            </a>
          </li>
        </ul>
      </div>
      <div className="footer-container-allrights">
        <h2>@ ALL RIGHTS RESERVED</h2>
      </div>
    </footer>
  );
}
