import React, { useEffect, useState } from "react";
import { albums } from "../Data/albums";

export default function Albums() {
  const [albumState, setAlbumState] = useState(albums);
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, []);

  const dateSort = (element) => {
    setAlbumState((prev) => {
      const sorted = prev.sort(
        (element1, element2) => element1[element] - element2[element]
      );
      if (element === "rating") {
        sorted.reverse();
      }
      return [...sorted];
    });
  };

  return (
    <div>
      <div>
        <select
          onChange={(e) => dateSort(e.target.value)}
          className="sorting-group"
        >
          <option value="" selected disabled hidden>
            sort by
          </option>
          <option value="year">
            year
          </option>
          <option value="rating">
            rating
          </option>
        </select>
      </div>
      <div className="main-container-photo-frame">
        {albumState.map((element, index) => (
          <div key={index} className="main-container-photo-frame-item">
            <img className="main-container-photo-frame-item-img" src={element.cover} alt={`${element.name}Cover`} />
            <div className="main-container-photo-frame-item-hover">
              <ul className="main-container-photo-frame-item-hover-list">
                {element.songs.map((song, indexli) => (
                  <li
                    key={indexli}
                    className="main-container-photo-frame-item-hover-list-item"
                  >
                    <p>{`${indexli + 1}. ${song}`}</p>
                  </li>
                ))}
              </ul>
            </div>
            <p>{element.name}</p>
            <p>{element.year}</p>
          </div>
        ))}
      </div>
    </div>
  );
}
