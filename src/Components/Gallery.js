import React,{useState} from "react";
import { gallery } from "../Data/gallery";

export default function Gallery() {
    const [photos,setPhotos] = useState(gallery.slice(0,12));
    const [activeLi,setActiveLi] = useState(1);

    const handleClick = (counter) => {
        counter = Number.parseInt(counter);
        setActiveLi(counter)
        setPhotos(gallery.slice(counter*12-12,10*counter+2*counter));

    }
  return (
    <div>
      <ul className="breadcrumbs-container">
        <li onClick={(e)=>handleClick(e.target.innerText)} className={activeLi===1?"active":""}>1</li>
        <li onClick={(e)=>handleClick(e.target.innerText)} className={activeLi===2?"active":""}>2</li>
        <li onClick={(e)=>handleClick(e.target.innerText)} className={activeLi===3?"active":""}>3</li>
        <li onClick={(e)=>handleClick(e.target.innerText)} className={activeLi===4?"active":""}>4</li>
        <li onClick={(e)=>handleClick(e.target.innerText)} className={activeLi===5?"active":""}>5</li>
      </ul>
      <div className="main-container-gallery">
        {photos.map((el,index) => (
          <div key={index} className="main-container-gallery-item">
            <img src={el.picture} />
          </div>
        ))}
      </div>
    </div>
  );
}
