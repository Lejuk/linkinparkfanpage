export const albums = [
    {
        name:"Hybrid Theory",
        year:2000,
        rating:7.5,
        cover:"https://vinylclub.com.ua/inc/uploads/2018/11/12a791a4-e9b9-11e8-80e4-00163e233d69-1.jpg",
        songs: ['Papercut','One Step Closer','With You', 'Points of Authority','Crawling','Runaway','By Myself','In the End','A Place for My Head','Forgotten','Cure for the Itch','Pushing Me Away']
        
    },
    {
        name:"One More Light",
        year:2017,
        rating:6.4,
        cover:"https://upload.wikimedia.org/wikipedia/ru/f/f8/One_More_Light.jpg",
        songs: ['Nobody Can Save Me', 'Good Goodbye (Ft. Pusha T & Stormzy)','Talking to Myself','Battle Symphony','Invisible','Heavy (Ft. Kiiara)','Sorry for Now','Halfway Right','One More Light','Sharp Edges']
    },
    {
        name:"The Hunting Party",
        year:2014,
        rating:9,
        cover:"https://upload.wikimedia.org/wikipedia/ru/3/3c/The_Hunting_Party_2014.jpg",
        songs: ['Keys to the Kingdom','All for Nothing (Ft. Page Hamilton)','Guilty All the Same (Ft. Rakim)', 'The Summoning','War','Wastelands','Until It`s Gone','Rebellion (Ft. Daron Malakian)','Mark the Graves','Drawbar (Ft. Tom Morello)','Final Masquerade','A Line in the Sand']
    },
    {
        name:"Minutes to Midnight",
        year:2007,
        rating:6.9,
        cover:"https://vinyla.com/files/products/linkin-park-minutes-to-midnight.800x800.png?0b94239673f53554bfe9bc1cc0a2f291",
        songs: ['Wake','Given Up','Leave Out All the Rest','Bleed It Out','Shadow of the Day','What I`ve Done','Hands Held High','No More Sorrow','Valentine`s Day','In Between','In Pieces','The Little Things Give You Away','No Roads Left','Across the Line']
    },
    {
        name:"Meteora",
        year:2003,
        rating:8.2,
        cover:"https://i.pinimg.com/originals/71/b3/ea/71b3eafd3b0ef3e0a50ad0945e911f3c.jpg",
        songs: ['Foreword', 'Don`t Stay', 'Somewhere I Belong', 'Lying from You','Hit the Floor','Easier to Run','Faint' ,'Figure' ,'Breaking the Habit', 'From the Inside', 'Nobody`s Listening','Session','Numb']
    },
    {
        name:"Living Things",
        year:2012,
        rating:7.6,
        cover:"https://upload.wikimedia.org/wikipedia/ru/0/00/Living_Things.jpg",
        songs: ['LOST IN THE ECHO','IN MY REMAINS','BURN IT DOWN','LIES GREED MISERY','I’LL BE GONE','CASTLE OF GLASS', 'VICTIMIZED' ,'ROADS UNTRAVELED','SKIN TO BONE','UNTIL IT BREAKS','TINFOIL', 'POWERLESS']
    },
    {
        name:"Xero",
        year:1997,
        rating:8.6,
        cover:"https://i.ytimg.com/vi/B3egnoj9nMU/hqdefault.jpg",
        songs: ['RHINESTONE','READING MY EYES','FUSE','STICK N MOVE']
    },
    {
        name:"A Thousand Suns",
        year:2010,
        rating:7.9,
        cover:"https://upload.wikimedia.org/wikipedia/ru/b/b9/ATS_lpblast.jpg",
        songs: ['THE REQUIEM','THE RADIANCE','BURNING IN THE SKIES','EMPTY SPACES', 'ROBOT BOY', 'WAITING FOR THE END', 'BLACKOUT', 'FALLOUT', 'THE CATALYST', 'THE MESSENGER']
    },
    {
        name:"Reanimation",
        year:2002,
        rating:6.4,
        cover:"https://a3-images.myspacecdn.com/images03/2/1eaedce67bcf41b69e4c3e9869c43112/600x600.jpg",
        songs: ['OPENING','PTS.OF.ARTHY','ENTH E ND','CHALI', 'HIGH VOLTAGE', 'WITH YOU', 'PAPERCUT', 'STEF', 'BY MYSELF', 'CRAWLING']
    }

  ]
  