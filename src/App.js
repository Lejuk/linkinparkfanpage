import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MainScreen from "./Components/MainScreen";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import Gallery from "./Components/Gallery";
import Albums from "./Components/Albums";
import UpcomingEvents from "./Components/UpcomingEvents";
import Contacts from "./Components/Contacts";
function App() {
  return (
    <div className="content">
      <Router>
     <Header/>
        <Switch>
          <Route exact path="/">
            <MainScreen />
          </Route>
          <Route path="/gallery">
            <Gallery/>
          </Route>
          <Route path="/albums">
            <Albums/>
          </Route>
          <Route path="/upcoming events">
            <UpcomingEvents/>
          </Route>
          <Route path="/contacts">
            <Contacts/>
          </Route>
        </Switch>
      <Footer />
      </Router>
    </div>
  );
}

export default App;
